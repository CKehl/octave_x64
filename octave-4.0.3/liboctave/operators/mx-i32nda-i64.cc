// DO NOT EDIT -- generated by mk-ops
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include "Array-util.h"
#include "mx-i32nda-i64.h"
#include "mx-op-defs.h"
#include "boolMatrix.h"
#include "boolNDArray.h"
#include "int32NDArray.h"
#include "oct-inttypes.h"
NDS_CMP_OPS (int32NDArray, octave_int64)
NDS_BOOL_OPS (int32NDArray, octave_int64)
