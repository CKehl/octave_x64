// DO NOT EDIT -- generated by mk-ops
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include "Array-util.h"
#include "mx-fnda-i16.h"
#include "mx-op-defs.h"
#include "boolMatrix.h"
#include "boolNDArray.h"
#include "int16NDArray.h"
#include "fNDArray.h"
#include "oct-inttypes.h"
NDS_BIN_OPS (int16NDArray, FloatNDArray, octave_int16)
NDS_CMP_OPS (FloatNDArray, octave_int16)
NDS_BOOL_OPS (FloatNDArray, octave_int16)
