// DO NOT EDIT -- generated by mk-ops
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include "Array-util.h"
#include "mx-i64nda-i16nda.h"
#include "mx-op-defs.h"
#include "boolMatrix.h"
#include "boolNDArray.h"
#include "int64NDArray.h"
#include "int16NDArray.h"
NDND_CMP_OPS (int64NDArray, int16NDArray)
NDND_BOOL_OPS (int64NDArray, int16NDArray)
