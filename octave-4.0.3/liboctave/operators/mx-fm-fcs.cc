// DO NOT EDIT -- generated by mk-ops
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include "Array-util.h"
#include "mx-fm-fcs.h"
#include "mx-op-defs.h"
#include "boolMatrix.h"
#include "fCMatrix.h"
#include "fMatrix.h"
#include "oct-cmplx.h"
MS_BIN_OPS (FloatComplexMatrix, FloatMatrix, FloatComplex)
MS_CMP_OPS (FloatMatrix, FloatComplex)
MS_BOOL_OPS (FloatMatrix, FloatComplex)
