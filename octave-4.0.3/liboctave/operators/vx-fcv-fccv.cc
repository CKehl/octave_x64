// DO NOT EDIT -- generated by mk-ops
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include "Array-util.h"
#include "vx-fcv-fccv.h"
#include "mx-op-defs.h"
#include "fCColVector.h"
#include "fColVector.h"
VV_BIN_OPS (FloatComplexColumnVector, FloatColumnVector, FloatComplexColumnVector)
