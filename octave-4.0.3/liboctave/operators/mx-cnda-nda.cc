// DO NOT EDIT -- generated by mk-ops
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include "Array-util.h"
#include "mx-cnda-nda.h"
#include "mx-op-defs.h"
#include "boolMatrix.h"
#include "boolNDArray.h"
#include "CNDArray.h"
#include "dNDArray.h"
NDND_BIN_OPS (ComplexNDArray, ComplexNDArray, NDArray)
NDND_CMP_OPS (ComplexNDArray, NDArray)
NDND_BOOL_OPS (ComplexNDArray, NDArray)
