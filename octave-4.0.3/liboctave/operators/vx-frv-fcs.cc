// DO NOT EDIT -- generated by mk-ops
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include "Array-util.h"
#include "vx-frv-fcs.h"
#include "mx-op-defs.h"
#include "fCRowVector.h"
#include "fRowVector.h"
#include "oct-cmplx.h"
VS_BIN_OPS (FloatComplexRowVector, FloatRowVector, FloatComplex)
