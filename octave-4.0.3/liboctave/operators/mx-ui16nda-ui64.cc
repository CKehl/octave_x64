// DO NOT EDIT -- generated by mk-ops
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include "Array-util.h"
#include "mx-ui16nda-ui64.h"
#include "mx-op-defs.h"
#include "boolMatrix.h"
#include "boolNDArray.h"
#include "uint16NDArray.h"
#include "oct-inttypes.h"
NDS_CMP_OPS (uint16NDArray, octave_uint64)
NDS_BOOL_OPS (uint16NDArray, octave_uint64)
