// DO NOT EDIT -- generated by mk-ops
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include "Array-util.h"
#include "mx-ui8-ui16nda.h"
#include "mx-op-defs.h"
#include "boolMatrix.h"
#include "boolNDArray.h"
#include "oct-inttypes.h"
#include "uint16NDArray.h"
SND_CMP_OPS (octave_uint8, uint16NDArray)
SND_BOOL_OPS (octave_uint8, uint16NDArray)
