// DO NOT EDIT -- generated by mk-ops
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include "Array-util.h"
#include "mx-cm-s.h"
#include "mx-op-defs.h"
#include "boolMatrix.h"
#include "CMatrix.h"
MS_BIN_OPS (ComplexMatrix, ComplexMatrix, double)
MS_CMP_OPS (ComplexMatrix, double)
MS_BOOL_OPS (ComplexMatrix, double)
