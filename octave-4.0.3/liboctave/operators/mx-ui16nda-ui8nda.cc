// DO NOT EDIT -- generated by mk-ops
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include "Array-util.h"
#include "mx-ui16nda-ui8nda.h"
#include "mx-op-defs.h"
#include "boolMatrix.h"
#include "boolNDArray.h"
#include "uint16NDArray.h"
#include "uint8NDArray.h"
NDND_CMP_OPS (uint16NDArray, uint8NDArray)
NDND_BOOL_OPS (uint16NDArray, uint8NDArray)
