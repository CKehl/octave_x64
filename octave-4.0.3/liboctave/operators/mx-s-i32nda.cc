// DO NOT EDIT -- generated by mk-ops
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include "Array-util.h"
#include "mx-s-i32nda.h"
#include "mx-op-defs.h"
#include "boolMatrix.h"
#include "boolNDArray.h"
#include "int32NDArray.h"
SND_BIN_OPS (int32NDArray, double, int32NDArray)
SND_CMP_OPS (double, int32NDArray)
SND_BOOL_OPS (double, int32NDArray)
