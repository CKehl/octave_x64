// DO NOT EDIT -- generated by mk-ops
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include "Array-util.h"
#include "mx-cs-dm.h"
#include "mx-op-defs.h"
#include "CDiagMatrix.h"
#include "oct-cmplx.h"
#include "dDiagMatrix.h"
SDM_BIN_OPS (ComplexDiagMatrix, Complex, DiagMatrix)
