// DO NOT EDIT -- generated by mk-ops
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include "Array-util.h"
#include "mx-fm-fcm.h"
#include "mx-op-defs.h"
#include "boolMatrix.h"
#include "fCMatrix.h"
#include "fMatrix.h"
MM_BIN_OPS (FloatComplexMatrix, FloatMatrix, FloatComplexMatrix)
MM_CMP_OPS (FloatMatrix, FloatComplexMatrix)
MM_BOOL_OPS (FloatMatrix, FloatComplexMatrix)
