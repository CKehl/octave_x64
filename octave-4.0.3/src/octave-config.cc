// DO NOT EDIT!  Generated automatically from octave-config.in.cc by Make.
/*

Copyright (C) 2008-2015 Michael Goffioul

This file is part of Octave.

Octave is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation; either version 3 of the License, or (at your
option) any later version.

Octave is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with Octave; see the file COPYING.  If not, see
<http://www.gnu.org/licenses/>.

*/

#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif

#include <string>
#include <map>
#include <iostream>
#include <algorithm>
#include <cstdlib>

#ifndef OCTAVE_PREFIX
#define OCTAVE_PREFIX "/var/scratch/CIL-octave"
#endif

#include "shared-fcns.h"

static std::map<std::string,std::string> vars;

static std::string usage_msg = "usage: octave-config [options]";

static std::string help_msg =
"\n"
"Options:\n"
"\n"
"  -h, -?, --help        Print this message.\n"
"\n"
"  --m-site-dir          Print the name of the directory where Octave\n"
"                        expects to find locally installed .m files.\n"
"\n"
"  --oct-site-dir        Print the name of the directory where Octave\n"
"                        expects to find locally installed .oct files.\n"
"\n"
"  -p VAR, --print VAR   Print the value of the given configuration\n"
"                        variable VAR.  Recognized variables are:\n"
"\n"
"                          API_VERSION            LOCALARCHLIBDIR\n"
"                          ARCHLIBDIR             LOCALFCNFILEDIR\n"
"                          BINDIR                 LOCALOCTFILEDIR\n"
"                          CANONICAL_HOST_TYPE    LOCALSTARTUPFILEDIR\n"
"                          DATADIR                LOCALVERARCHLIBDIR\n"
"                          DATAROOTDIR            LOCALVERFCNFILEDIR\n"
"                          DEFAULT_PAGER          LOCALVEROCTFILEDIR\n"
"                          EXEC_PREFIX            MAN1DIR\n"
"                          EXEEXT                 MAN1EXT\n"
"                          FCNFILEDIR             MANDIR\n"
"                          IMAGEDIR               OCTDATADIR\n"
"                          INCLUDEDIR             OCTFILEDIR\n"
"                          INFODIR                OCTINCLUDEDIR\n"
"                          INFOFILE               OCTLIBDIR\n"
"                          LIBDIR                 PREFIX\n"
"                          LIBEXECDIR             SHLEXT\n"
"                          LOCALAPIARCHLIBDIR     STARTUPFILEDIR\n"
"                          LOCALAPIFCNFILEDIR     VERSION\n"
"                          LOCALAPIOCTFILEDIR\n"
"\n"
"  -v, --version         Print the Octave version number.\n"
"\n";

static void
initialize (void)
{
  vars["OCTAVE_HOME"] = get_octave_home ();
  vars["PREFIX"] = OCTAVE_PREFIX;

  vars["API_VERSION"] = "api-v50+";
  vars["CANONICAL_HOST_TYPE"] = "x86_64-pc-linux-gnu";
  vars["DEFAULT_PAGER"] = "less";
  vars["MAN1EXT"] = ".1";
  vars["VERSION"] = "4.0.3";

  vars["ARCHLIBDIR"] = subst_octave_home ("/var/scratch/CIL-octave/libexec/octave/4.0.3/exec/x86_64-pc-linux-gnu");
  vars["BINDIR"] = subst_octave_home ("/var/scratch/CIL-octave/bin");
  vars["DATADIR"] = subst_octave_home ("/var/scratch/CIL-octave/share");
  vars["DATAROOTDIR"] = subst_octave_home ("/var/scratch/CIL-octave/share");
  vars["EXEC_PREFIX"] = subst_octave_home ("/var/scratch/CIL-octave");
  vars["EXEEXT"] = subst_octave_home ("");
  vars["FCNFILEDIR"] = subst_octave_home ("/var/scratch/CIL-octave/share/octave/4.0.3/m");
  vars["IMAGEDIR"] = subst_octave_home ("/var/scratch/CIL-octave/share/octave/4.0.3/imagelib");
  vars["INCLUDEDIR"] = subst_octave_home ("/var/scratch/CIL-octave/include");
  vars["INFODIR"] = subst_octave_home ("/var/scratch/CIL-octave/share/info");
  vars["INFOFILE"] = subst_octave_home ("/var/scratch/CIL-octave/share/info/octave.info");
  vars["LIBDIR"] = subst_octave_home ("/var/scratch/CIL-octave/lib");
  vars["LIBEXECDIR"] = subst_octave_home ("/var/scratch/CIL-octave/libexec");
  vars["LOCALAPIARCHLIBDIR"] = subst_octave_home ("/var/scratch/CIL-octave/libexec/octave/api-v50+/site/exec/x86_64-pc-linux-gnu");
  vars["LOCALAPIFCNFILEDIR"] = subst_octave_home ("/var/scratch/CIL-octave/share/octave/site/api-v50+/m");
  vars["LOCALAPIOCTFILEDIR"] = subst_octave_home ("/var/scratch/CIL-octave/lib/octave/site/oct/api-v50+/x86_64-pc-linux-gnu");
  vars["LOCALARCHLIBDIR"] = subst_octave_home ("/var/scratch/CIL-octave/libexec/octave/site/exec/x86_64-pc-linux-gnu");
  vars["LOCALFCNFILEDIR"] = subst_octave_home ("/var/scratch/CIL-octave/share/octave/site/m");
  vars["LOCALOCTFILEDIR"] = subst_octave_home ("/var/scratch/CIL-octave/lib/octave/site/oct/x86_64-pc-linux-gnu");
  vars["LOCALSTARTUPFILEDIR"] = subst_octave_home ("/var/scratch/CIL-octave/share/octave/site/m/startup");
  vars["LOCALVERARCHLIBDIR"] = subst_octave_home ("/var/scratch/CIL-octave/libexec/octave/4.0.3/site/exec/x86_64-pc-linux-gnu");
  vars["LOCALVERFCNFILEDIR"] = subst_octave_home ("/var/scratch/CIL-octave/share/octave/4.0.3/site/m");
  vars["LOCALVEROCTFILEDIR"] = subst_octave_home ("/var/scratch/CIL-octave/lib/octave/4.0.3/site/oct/x86_64-pc-linux-gnu");
  vars["MAN1DIR"] = subst_octave_home ("/var/scratch/CIL-octave/share/man/man1");
  vars["MANDIR"] = subst_octave_home ("/var/scratch/CIL-octave/share/man");
  vars["OCTDATADIR"] = subst_octave_home ("/var/scratch/CIL-octave/share/octave/4.0.3/data");
  vars["OCTFILEDIR"] = subst_octave_home ("/var/scratch/CIL-octave/lib/octave/4.0.3/oct/x86_64-pc-linux-gnu");
  vars["OCTINCLUDEDIR"] = subst_octave_home ("/var/scratch/CIL-octave/include/octave-4.0.3/octave");
  vars["OCTLIBDIR"] = subst_octave_home ("/var/scratch/CIL-octave/lib/octave/4.0.3");
  vars["SHLEXT"] = subst_octave_home ("so");
  vars["STARTUPFILEDIR"] = subst_octave_home ("/var/scratch/CIL-octave/share/octave/4.0.3/m/startup");
}

int
main (int argc, char **argv)
{
  initialize ();

  if (argc == 1)
    {
      std::cout << usage_msg << std::endl;
      return 1;
    }

  for (int i = 1; i < argc; i++)
    {
      std::string arg (argv[i]);

      if (arg == "-h" || arg == "-?" || arg == "--help")
        {
          std::cout << usage_msg << std::endl;
          std::cout << help_msg;
          return 0;
        }
      else if (arg == "--m-site-dir")
        std::cout << vars["LOCALVERFCNFILEDIR"] << std::endl;
      else if (arg == "--oct-site-dir")
        std::cout << vars["LOCALVEROCTFILEDIR"] << std::endl;
      else if (arg == "-v" || arg == "--version")
        std::cout << vars["VERSION"] << std::endl;
      else if (arg == "-p" || arg == "--print")
        {
          if (i < argc-1)
            {
              arg = argv[++i];
              std::cout << vars[arg] << std::endl;
            }
          else
            {
              std::cerr << "octave-config: " << arg
                        << " options requires argument" << std::endl;
              return 1;
            }
        }
      else
        {
          std::cerr << "octave-config: unrecognized argument " << arg
                    << std::endl;
          return 1;
        }
    }

  return 0;
}
