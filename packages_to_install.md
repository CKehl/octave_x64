
install the following packages:
tcl-dev bison flex g++ gfortran cmake gperf f2c cmake-qt-gui autoconf libtool-bin cmake-curses-gui libqscintilla2-dev libpcre16-3 libpcre2-32-0 libpcre2-dev libpcre2-posix0 libpcre3 libpcre32-3 libpcre3-dev libz3-dev libhdf5-10 libhdf5-10 libhdf5-cpp-11 libhdf5-dev libhdf5-serial-dev hdf5-helpers hdf5-tools libfftw3-3 libfftw3-bin libfftw3-dev libfftw3-double3 libfftw3-long3 curl libcurl3 libcurl3-gnutls libcurl3-nss libcurlpp0 libcurlpp-dev libgraphicsmagick++1-dev libgraphicsmagick++-q16-12 libfreetype6 libfreetype6-dev libfltk1.3 libfltk1.3-dev libfltk1.3-compat-headers libfltk-gl1.3 libfltk-images1.3 libfltk-cairo1.3 libfltk-forms1.3 libgnuplot-iostream-dev gnuplot5 gnuplot5-data gnuplot5-qt libboost-all-dev libtermkey1 libtermkey-dev libreadline6 libreadline6-dev qt5-default qt5-qmake qttools5-dev-tools fontconfig fontconfig-config libfontconfig1 libfontconfig1-dev libosmesa6 libosmesa6-dev texinfo libcurl4-gnutls-dev libxft-dev libgl2ps-dev mercurial

HINTS for using GCC >= 6.0.0:

there has been a change in the linking system, hence instead of setting just the "LD_LIBRARY_PATH", we now need to set the following 3 environment variables:

LD_LIBRARY_PATH
LIBRARY_PATH
LD_RUN_PATH

Errors based on that are indicated by octave (during the configuration) not finding the CUDA libraries ("-lcuda" or -lcudart").

If configure is not able to find a library that is certainly installed, execute the following command (example: libGL; see https://askubuntu.com/questions/485157/cannot-find-lgl-but-ive-installed-the-nvidia-driver):

ldconfig -p | grep libGL.so

good output:
	libGL.so.1 (libc6,x86-64) => /usr/lib/x86_64-linux-gnu/libGL.so.1
	libGL.so.1 (libc6,x86-64) => /usr/lib/libGL.so.1
	libGL.so.1 (libc6) => /usr/lib/i386-linux-gnu/libGL.so.1
	libGL.so (libc6,x86-64) => /usr/lib/x86_64-linux-gnu/libGL.so
	libGL.so (libc6) => /usr/lib/i386-linux-gnu/libGL.so

if it doesn't find a good "libGL.so", it is possible the relative linking is broken.

execute then:

ls -lisa /usr/lib/x86_64-linux-gnu/ | grep libGL.so

good output:
5520502      0 lrwxrwxrwx   2 root root        14 Sep 17 21:53 libGL.so -> libGL.so.1.7.0
5520502      0 lrwxrwxrwx   2 root root        14 Sep 17 21:53 libGL.so.1 -> libGL.so.1.7.0
5508149    652 -rwxr-xr-x   1 root root    665720 Sep 17 21:53 libGL.so.1.7.0

if "libGL.so" is broken, then usually it would here refer to a library that is not there anymore. In that case, remove the "libGL.so" (which is only a link), and relink it correctly. Here for example:

sudo rm /usr/lib/x86_64-linux-gnu/libGL.so
sudo ln /usr/lib/x86_64-linux-gnu/libGL.so.1 /usr/lib/x86_64-linux-gnu/libGL.so

ALSO: in gcc >=6.0.0, the gnulib subpackage of octave is broken as it does not find "gnulib:floor()". In that case, you need to patch octave (see: https://savannah.gnu.org/bugs/?32444):

cd octave-4.0-3
patch -p1 < gcc-6-abs-overload.patch
patch -p1 < gcc-6-include-math-stdlib.patch

the files can be downloaded at:
https://savannah.gnu.org/bugs/download.php?file_id=42834
https://savannah.gnu.org/bugs/download.php?file_id=42835

Further information on external packages:
https://octave.org/doc/v4.0.0/External-Packages.html


Further instructions for compiling the 64-bit extension:
https://octave.org/doc/interpreter/Compiling-Octave-with-64_002dbit-Indexing.html (BEST GUIDE)
http://octave.org/doc/v4.0.0/Compiling-Octave-with-64_002dbit-Indexing.html
https://github.com/calaba/octave-3.8.2-enable-64-ubuntu-14.04
https://github.com/octave-de/GNU-Octave-enable-64
https://fossies.org/linux/octave/INSTALL.OCTAVE
http://matrix.umcs.lublin.pl/cgi-bin/info2www?(octave)Compiling+Octave+with+64-bit+Indexing



"configure invalid" problem:
regenerate the "configure" file by running autoreconf (part of autotools):

autoreconf -mif

more info see:
https://robots.thoughtbot.com/the-magic-behind-configure-make-make-install
https://www.gnu.org/software/autoconf/manual/autoconf-2.68/html_node/autoreconf-Invocation.html
